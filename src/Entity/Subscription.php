<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity
 */
class Subscription
{
    use TimestampableTrait;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $renewId;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $productId;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $changeDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $cancellationDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiresDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $purchaseDate;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $transactionId;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $active = false;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $provider;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Subscription
     */
    public function setId(int $id): Subscription
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRenewId(): ?string
    {
        return $this->renewId;
    }

    /**
     * @param string|null $renewId
     * @return Subscription
     */
    public function setRenewId(?string $renewId): Subscription
    {
        $this->renewId = $renewId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param string|null $productId
     * @return Subscription
     */
    public function setProductId(?string $productId): Subscription
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return Subscription
     */
    public function setStatus(?string $status): Subscription
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getChangeDate(): ?\DateTime
    {
        return $this->changeDate;
    }

    /**
     * @param \DateTime|null $changeDate
     * @return Subscription
     */
    public function setChangeDate(?\DateTime $changeDate): Subscription
    {
        $this->changeDate = $changeDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCancellationDate(): ?\DateTime
    {
        return $this->cancellationDate;
    }

    /**
     * @param \DateTime|null $cancellationDate
     * @return Subscription
     */
    public function setCancellationDate(?\DateTime $cancellationDate): Subscription
    {
        $this->cancellationDate = $cancellationDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiresDate(): ?\DateTime
    {
        return $this->expiresDate;
    }

    /**
     * @param \DateTime|null $expiresDate
     * @return Subscription
     */
    public function setExpiresDate(?\DateTime $expiresDate): Subscription
    {
        $this->expiresDate = $expiresDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPurchaseDate(): ?\DateTime
    {
        return $this->purchaseDate;
    }

    /**
     * @param \DateTime|null $purchaseDate
     * @return Subscription
     */
    public function setPurchaseDate(?\DateTime $purchaseDate): Subscription
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    /**
     * @param string|null $transactionId
     * @return Subscription
     */
    public function setTransactionId(?string $transactionId): Subscription
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Subscription
     */
    public function setActive(bool $active): Subscription
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProvider(): ?string
    {
        return $this->provider;
    }

    /**
     * @param string|null $provider
     * @return Subscription
     */
    public function setProvider(?string $provider): Subscription
    {
        $this->provider = $provider;

        return $this;
    }
}