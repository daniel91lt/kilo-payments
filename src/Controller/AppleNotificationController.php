<?php

namespace App\Controller;

use App\Constant\PaymentProvider;
use App\Constant\SubscriptionEvents;
use App\Event\CancelEvent;
use App\Event\FailedRenewEvent;
use App\Event\InitialBuyEvent;
use App\Event\RenewEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/apple-notifaction", name="apple_")
 */
class AppleNotificationController extends AbstractController
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * DefaultController constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        $type = $request->get('notification_type');

        switch ($type) {
            case SubscriptionEvents::INITIAL_BUY:
                $this->initialBuy($request);
                break;
            case SubscriptionEvents::RENEW:
                $this->renew($request);
                break;
            case SubscriptionEvents::FAILED_RENEW:
                $this->failedRenew($request);
                break;
            case SubscriptionEvents::CANCEL:
                $this->cancel($request);
                break;
            default:
                return new JsonResponse('no action');
        }
    }

    private function initialBuy(Request $request)
    {
        /** @TODO: add data parsing and validation */
        $initialBuyEvent = (new InitialBuyEvent())
            ->setProvider(PaymentProvider::APPLE);

        $this->eventDispatcher->dispatch($initialBuyEvent);

        return new JsonResponse('ok');
    }


    public function renew(Request $request)
    {
        /** @TODO: add data parsing and validation */
        $renewEvent = (new RenewEvent())
            ->setProvider(PaymentProvider::APPLE);

        $this->eventDispatcher->dispatch($renewEvent);

        return new JsonResponse('ok');
    }


    public function failedRenew(Request $request)
    {
        /** @TODO: add data parsing and validation */
        $failedRenewEvent = (new FailedRenewEvent())
            ->setProvider(PaymentProvider::APPLE);

        $this->eventDispatcher->dispatch($failedRenewEvent);

        return new JsonResponse('ok');
    }


    public function cancel(Request $request)
    {
        /** @TODO: add data parsing and validation */
        $cancelEvent = (new CancelEvent())
            ->setProvider(PaymentProvider::APPLE);

        $this->eventDispatcher->dispatch($cancelEvent);

        return new JsonResponse('ok');
    }
}


