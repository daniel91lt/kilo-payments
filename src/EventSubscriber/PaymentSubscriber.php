<?php


namespace App\EventSubscriber;


use App\Constant\PaymentProvider;
use App\Event\CancelEvent;
use App\Event\FailedRenewEvent;
use App\Event\InitialBuyEvent;
use App\Event\RenewEvent;
use App\Event\SubscriptionEventInterface;
use App\Service\AbstractPaymentService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentSubscriber implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PaymentSubscriber constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            InitialBuyEvent::class => ['onInitialBuy', 0],
            RenewEvent::class => ['onRenew', 0],
            FailedRenewEvent::class => ['onFailedRenew', 0],
            CancelEvent::class => ['onCancel', 0],
        ];
    }

    /**
     * @param InitialBuyEvent $initialBuyEvent
     */
    public function onInitialBuy(InitialBuyEvent $initialBuyEvent)
    {
        try {
            $service = $this->getPaymentService($initialBuyEvent);
            $service->createSubscription($initialBuyEvent);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    /**
     * @param RenewEvent $renewEvent
     */
    public function onRenew(RenewEvent $renewEvent)
    {
        try {
            $service = $this->getPaymentService($renewEvent);
            $service->renewSubscription($renewEvent);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    /**
     * @param FailedRenewEvent $failedRenewEvent
     */
    public function onFailedRenew(FailedRenewEvent $failedRenewEvent)
    {
        try {
            $service = $this->getPaymentService($failedRenewEvent);
            $service->failedRenew($failedRenewEvent);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    /**
     * @param CancelEvent $cancelEvent
     * @throws \Exception
     */
    public function onCancel(CancelEvent $cancelEvent)
    {
        try {
            $service = $this->getPaymentService($cancelEvent);
            $service->cancelSubscription($cancelEvent);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    /**
     * @param SubscriptionEventInterface $event
     * @return AbstractPaymentService
     * @throws \Exception
     */
    private function getPaymentService(SubscriptionEventInterface $event)
    {
        $provider = $event->getProvider();
        $serviceAlias = PaymentProvider::getServiceAlias($provider);

        if (empty($serviceAlias)) {
            throw new \Exception('Unknown provider');
        }

        /** @TODO: add more providers */
        switch ($provider) {
            case PaymentProvider::APPLE:
                $service = $this->container->get($serviceAlias);
                break;
        }

        return $service;
    }
}