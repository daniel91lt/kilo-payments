<?php


namespace App\Constant;


final class SubscriptionStatus
{
    const ACTIVE = 'active';
    const EXPIRED = 'expired';
    const FAILED = 'failed';
    const CANCELED = 'canceled';
}
