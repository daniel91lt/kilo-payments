<?php


namespace App\Constant;


final class SubscriptionEvents
{
    // APPLE
    const INITIAL_BUY = 'INITIAL_BUY';
    const RENEW = 'DID_RENEW';
    const FAILED_RENEW = 'DID_FAIL_TO_RENEW';
    const CANCEL = 'CANCEL';

    // TODO: Add more event types
}
