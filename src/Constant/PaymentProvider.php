<?php

namespace App\Constant;

/**
 * Class Locale
 * @package App\Constant
 */
final class PaymentProvider
{
    const APPLE = 'apple';
    const STRIPE = 'stripe';
    const PAYPAL = 'paypal';

    /** @TODO: add more services */
    public static $services = [
        self::APPLE => 'app.apple.payment.service',
        self::STRIPE => 'app.stripe.payment.service',
    ];

    /**
     * @param string $provider
     * @return string
     */
    public static function getServiceAlias($provider)
    {
        return isset(self::$services[$provider]) ? self::$services[$provider] : '';
    }
}