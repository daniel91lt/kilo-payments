<?php


namespace App\Event;


class RenewEvent extends AbstractSubscriptionEvent implements ExistingSubsriptionInteface
{
    /**
     * @var \DateTime|null
     */
    private $expiresDate;

    /**
     * @return \DateTime|null
     */
    public function getExpiresDate(): ?\DateTime
    {
        return $this->expiresDate;
    }

    /**
     * @param \DateTime|null $expiresDate
     * @return RenewEvent
     */
    public function setExpiresDate(?\DateTime $expiresDate): RenewEvent
    {
        $this->expiresDate = $expiresDate;

        return $this;
    }

    public function getSubscription()
    {
        // TODO: Implement getSubscription() method.
    }
}