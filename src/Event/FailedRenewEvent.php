<?php


namespace App\Event;


class FailedRenewEvent extends AbstractSubscriptionEvent implements ExistingSubsriptionInteface
{
    /**
     * @var \DateTime|null
     */
    private $expiresDate;

    /**
     * @return \DateTime|null
     */
    public function getExpiresDate(): ?\DateTime
    {
        return $this->expiresDate;
    }

    /**
     * @param \DateTime|null $expiresDate
     * @return FailedRenewEvent
     */
    public function setExpiresDate(?\DateTime $expiresDate): FailedRenewEvent
    {
        $this->expiresDate = $expiresDate;

        return $this;
    }

    public function getSubscription()
    {
        // TODO: Implement getSubscription() method.
    }
}