<?php


namespace App\Event;


use Symfony\Contracts\EventDispatcher\Event;


abstract class AbstractSubscriptionEvent extends Event implements SubscriptionEventInterface
{
    /**
     * @var string
     */
    private $renewId;

    /**
     * @var string
     */
    private $productId;

    /**
     * @var string
     */
    private $transactionId;

    /**
     * @var string
     */
    private $provider;

    /**
     * @return string
     */
    public function getRenewId(): string
    {
        return $this->renewId;
    }

    /**
     * @param string $renewId
     * @return AbstractSubscriptionEvent
     */
    public function setRenewId(string $renewId): AbstractSubscriptionEvent
    {
        $this->renewId = $renewId;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     * @return AbstractSubscriptionEvent
     */
    public function setProductId(string $productId): AbstractSubscriptionEvent
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     * @return AbstractSubscriptionEvent
     */
    public function setTransactionId(string $transactionId): AbstractSubscriptionEvent
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     * @return AbstractSubscriptionEvent
     */
    public function setProvider(string $provider): AbstractSubscriptionEvent
    {
        $this->provider = $provider;

        return $this;
    }
}