<?php


namespace App\Event;

/**
 * Class InitialBuy
 *
 * @package App\Event
 */
class InitialBuyEvent extends AbstractSubscriptionEvent
{
    /**
     * @var \DateTime|null
     */
    private $purchaseDate;

    /**
     * @var \DateTime|null
     */
    private $expiresDate;

    /**
     * @return \DateTime|null
     */
    public function getPurchaseDate(): ?\DateTime
    {
        return $this->purchaseDate;
    }

    /**
     * @param \DateTime|null $purchaseDate
     * @return InitialBuyEvent
     */
    public function setPurchaseDate(?\DateTime $purchaseDate): InitialBuyEvent
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiresDate(): ?\DateTime
    {
        return $this->expiresDate;
    }

    /**
     * @param \DateTime|null $expiresDate
     * @return InitialBuyEvent
     */
    public function setExpiresDate(?\DateTime $expiresDate): InitialBuyEvent
    {
        $this->expiresDate = $expiresDate;

        return $this;
    }
}