<?php


namespace App\Event;


class CancelEvent extends AbstractSubscriptionEvent implements ExistingSubsriptionInteface
{
    /**
     * @var \DateTime|null
     */
    private $cancellationDate;

    /**
     * @var \DateTime|null
     */
    private $expiresDate;

    /**
     * @return \DateTime|null
     */
    public function getCancellationDate(): ?\DateTime
    {
        return $this->cancellationDate;
    }

    /**
     * @param \DateTime|null $cancellationDate
     * @return CancelEvent
     */
    public function setCancellationDate(?\DateTime $cancellationDate): CancelEvent
    {
        $this->cancellationDate = $cancellationDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiresDate(): ?\DateTime
    {
        return $this->expiresDate;
    }

    /**
     * @param \DateTime|null $expiresDate
     * @return CancelEvent
     */
    public function setExpiresDate(?\DateTime $expiresDate): CancelEvent
    {
        $this->expiresDate = $expiresDate;

        return $this;
    }

    public function getSubscription()
    {
        // TODO: Implement getSubscription() method.
    }
}