<?php


namespace App\Event;

/**
 * Interface SubscriptionEventInterface
 *
 * @package App\Event
 */
interface SubscriptionEventInterface
{
    /**
     * @return string
     */
    public function getRenewId();

    /**
     * @return string
     */
    public function getProductId();

    /**
     * @return string
     */
    public function getTransactionId();

    /**
     * @return string
     */
    public function getProvider();
}