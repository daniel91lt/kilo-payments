<?php


namespace App\Event;

use App\Entity\Subscription;

/**
 * Interface ExistingSubsriptionInteface
 *
 * @package App\Event
 */
interface ExistingSubsriptionInteface
{
    /**
     * @return Subscription
     */
    public function getSubscription();

    /**
     * @return \DateTime|null
     */
    public function getExpiresDate();
}