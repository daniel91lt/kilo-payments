<?php


namespace App\Service;

use App\Entity\Subscription;
use App\Event\CancelEvent;
use App\Event\ExistingSubsriptionInteface;
use App\Event\InitialBuyEvent;
use App\Event\SubscriptionEventInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ApplePaymentService
 *
 * @package App\Service
 */
class ApplePaymentService extends AbstractPaymentService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AbstractPaymentService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param InitialBuyEvent $event
     * @return Subscription
     */
    public function createSubscription(SubscriptionEventInterface $event)
    {
        $subscription = parent::createSubscription($event);

        // Example of provider logic
        $subscription
            ->setPurchaseDate($event->getPurchaseDate())
            ->setExpiresDate($event->getExpiresDate());

        $this->entityManager->persist($subscription);
        $this->entityManager->flush();

        return $subscription;
    }

    /**
     * @param CancelEvent $event
     * @return Subscription
     */
    public function cancelSubscription(ExistingSubsriptionInteface $event)
    {
        $subscription = parent::cancelSubscription($event);

        $subscription
            ->setCancellationDate($event->getCancellationDate());

        $this->entityManager->flush();

        return $subscription;
    }
}