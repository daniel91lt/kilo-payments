<?php


namespace App\Service;

use App\Constant\SubscriptionStatus;
use App\Entity\Subscription;
use App\Event\ExistingSubsriptionInteface;
use App\Event\SubscriptionEventInterface;

abstract class AbstractPaymentService
{
    /**
     * @param SubscriptionEventInterface $event
     * @return Subscription
     */
    public function createSubscription(SubscriptionEventInterface $event)
    {
        $subscription = (new Subscription())
            ->setProductId($event->getProductId())
            ->setRenewId($event->getRenewId())
            ->setProvider($event->getProvider())
            ->setTransactionId($event->getTransactionId())
            ->setActive(true);

        return $subscription;
    }

    /**
     * @param ExistingSubsriptionInteface $event
     * @return Subscription
     */
    public function renewSubscription(ExistingSubsriptionInteface $event)
    {
        $subscription = $event->getSubscription();

        $subscription
            ->setExpiresDate($event->getExpiresDate());

        return $subscription;
    }

    /**
     * @param ExistingSubsriptionInteface $event
     * @return Subscription
     */
    public function failedRenew(ExistingSubsriptionInteface $event)
    {
        $subscription = $event->getSubscription();

        $subscription
            ->setStatus(SubscriptionStatus::FAILED);

        return $subscription;
    }

    /**
     * @param ExistingSubsriptionInteface $event
     * @return Subscription
     */
    public function cancelSubscription(ExistingSubsriptionInteface $event)
    {
        $subscription = $event->getSubscription();

        $subscription
            ->setStatus(SubscriptionStatus::CANCELED)
            ->setActive(false);

        return $subscription;
    }

    public function createTransaction()
    {
        // TODO: create transaction
    }
}