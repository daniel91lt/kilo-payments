<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210310140800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE subscription (id INT AUTO_INCREMENT NOT NULL, renew_id VARCHAR(255) DEFAULT NULL, product_id VARCHAR(255) DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, change_date DATETIME DEFAULT NULL, cancellation_date DATETIME DEFAULT NULL, expires_date DATETIME DEFAULT NULL, purchase_date DATETIME DEFAULT NULL, transaction_id VARCHAR(255) DEFAULT NULL, active TINYINT(1) NOT NULL, provider VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE subscription');
    }
}
